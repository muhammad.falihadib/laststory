from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.home, name='home'),
    path('books/', views.books, name='books'),
    path('signup/', views.signUpPage, name='signup'),
    path('login/', views.loginPage, name='login'),
    path('logout/', views.logoutPage, name='logout'),
]