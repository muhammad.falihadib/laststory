from django.test import TestCase, Client

# Create your tests here.
class testStory9(TestCase):
    def test_url_home(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_books(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_url_login(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_url_signup(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)

    def test_template_home(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_template_login(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_template_signup(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response, 'signup.html')