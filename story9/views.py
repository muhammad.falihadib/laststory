from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.http import JsonResponse, request
import requests
import json

# Create your views here.
def home(request):
    return render(request, 'home.html')

def signUpPage(request):
	if request.user.is_authenticated:
		return redirect('/')
	else:
		form = UserCreationForm(request.POST or None)
		if (form.is_valid() and request.method == 'POST'):
			form.save()

			return redirect('/login')

		response = {'form' : form}
		return render(request, 'signup.html', response)

def loginPage(request):
	if request.user.is_authenticated:
		return redirect('/')
	else:
		if request.method == 'POST':
			username = request.POST.get('username')
			password =request.POST.get('password')

			user = authenticate(request, username=username, password=password)

			if user is not None:
				login(request, user)
				return redirect('/')
			else:
				messages.info(request, 'Username OR password is incorrect')

		return render(request, 'login.html')

def logoutPage(request):
	logout(request)
	return redirect('/login')

def books(request):
    return render(request, 'books.html')

def search_data(request):
	arg = request.GET['q']
	url_target = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
	r = requests.get(url_target)
	data = json.loads(r.content)

	if not request.user.is_authenticated:
		index = 0
		for x in data['items']:
			del data['items'][index]['volumeInfo']['imageLinks']['thumbnail']
			index += 1

	return JsonResponse(data, safe=False)